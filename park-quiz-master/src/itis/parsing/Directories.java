package itis.parsing;

public class Directories
{
    public static final String MAIN_FILES_FOLDER_PATH = "src/itis.parsing";
    public static final String DATABASE_FOLDER_PATH = MAIN_FILES_FOLDER_PATH + ".resources";
    public static final String PACKAGE_PREFIX = "itis.parsing";

    public static final String ID_KEY = "title";
    public static final String CLASS_KEY = "class";
    public static final String KEY_VALUE_SEPARATOR = ":";
    public static final String ENTITY_SEPARATOR = "***";

}
